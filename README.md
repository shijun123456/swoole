# Swoole的直播适配Thinkphp5.1代码

#### 项目介绍
Thinkphp5.1+Swoole2.1.3+Nginx+php7.2.3
Swoole_Webscoket_server
Swoole_Tcp_server
Swoole_Http_server
Task,Swoole_Table

#### 软件架构
软件架构说明


#### 安装教程

1. 放到 网站的nginx或者apache的webroot路径


#### 使用说明

1.服务器安装 swoole需要的 swoole,hpreis 
2. 切换到 thinphp/server php swoole_webscoket_server   
3. 如果开启的是 http 服务器 php thinkphp/server/php_swoole_http_server 
4.修改 nginx.conf 的 静态资源路径

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)